﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>City WEB</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Dropzone Css -->
    <link href="/js/plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- Bootstrap Core Css -->
    <link href="/js/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="/js/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/js/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="/js/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="/css/themes/all-themes.css" rel="stylesheet" />
    <link href="/css/imagestyle.css" rel="stylesheet" />
    <link rel='stylesheet' href='/css/simplelightbox.min.css' type='text/css'>

    <style>

        .ul .li{
            position:relative;float:left;
            width:100px;height:100px;padding:10px;list-style:none;
        }
        /*permet de créer deux lignes*/
       .ul .li:nth-child(4n){clear:left;}

        .ul .li a{
            display:block;overflow:hidden;
            width:100px;height:100px;
            margin-top:0px;margin-left:0px;
            border:0px solid rgba(0,0,0,0.7);
            transition-property:width,height,margin,z-index,border;
            transition-duration:0.4s;
        }

        .ul .li a:hover{
            position:absolute;
            width:200px;height:200px;
            margin-top:-56px;margin-left:-56px;
            z-index:100;
            border:6px solid rgba(0,0,0,0.7);
        }

        .ul .li a img{
            position:static;
            z-index:20;
            width: 25px;
            height:25px;
            transition-property:width,height,z-index;
            transition-duration:0.4s;
        }

        .ul .li a:hover img{
            width:200px;height:200px;
            z-index:100;
        }
    </style>
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand">City Admin</a>
            </div>
            </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
                    <div class="email"> {{ Auth::user()->email }}</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">

                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>

                        </ul>
                    </div>
                </div>
                </div>
            <!-- #User Info -->
            <!-- Menu -->
    <div class="menu">
                <ul class="list">
                    <li class="header">Menu</li>


                       <li class="active">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Gestion Région</span>
                        </a>

                        <ul class="ml-menu">
                            <li class="active">
                                <a href="addregion">Ajouter Région</a>
                            </li>
                            <li class="active">
                                <a href="listerregion">Lister Région</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                    
                   
            </div>
            <!-- end Menu -->
        </aside>
        <!-- end  Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
           
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Liste des régions
                            </h2>
                            <ul class="header-dropdown m-r--5">
                            </ul>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>

                                        <tr>
                                            <th>id</th>
                                            <th>Nom</th>
                                            <th>Description</th>
                                            <th>histoire</th>
                                            <th>specialité</th>
                                            <th>photo</th>
                                            <th>setting</th>
                                            <th>setting</th>
                                            <th hidden>Afficher</th>

                                           
                                        </tr>
                                    </thead>
                                  
                                    <tbody>
                                    @foreach($region as $myregion)
                                        <tr>
                                            <td>{{$myregion->id}}</td>
                                            <td>{{$myregion->nom}}</td>
                                            <td>{{$myregion->description}}</td>
                                            <td>{{$myregion->histoire}}</td>
                                            <td>{{$myregion->specialite}}</td>
                                            <td>
                                             @foreach($myregion->photo as $ph)
                                                <ul class="ul">
                                                    <li class="li"   class="col-sm-2">
                                                        <a href="uploads/{{$ph->nomphoto}}" class="imghvr-hinge-right figure">
                                                            <img  src="uploads/{{$ph->nomphoto}}" alt=""  />
                                                        </a>
                                                    </li>
                                                </ul>
                                                @endforeach


                                            </td>

                     <td>

                         <button  class="btn btn-primary" data-target="#myModal" data-toggle="modal"
                                                   data-region-id='<?php echo  $myregion->id;?>'
                                                   data-region-des='<?php echo $myregion->description;?>'
                                                   data-region-his='<?php echo $myregion->histoire;?>'
                                                   data-region-spe='<?php echo $myregion->specialite;?>'
                                                   data-region-nom='<?php echo $myregion->nom;?>'>Editer</button></td>

                                            <td><button
                                                   data-target="#us6-dialog" data-toggle="modal"
                                                   data-region-id='<?php echo $myregion->id;?>'
                                                    id="deleteButton" class="btn btn-danger">Delete</button>

                                            </td>
                                            <td hidden><a href="afficher/{{$myregion->id}}">Afficher</a></td>


                                     
                                        </tr>

                                        @endforeach
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- Modal update -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#4988c5;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modifier Region</h4>


                </div>
                <div class="modal-body">


                    <form  method="post" action="edit" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <label>Nom Region</label>
                        <div class="form-line">
                            <input  type="text" name="nomregion" readonly class="form-control no-resize">
                        </div>
                        <br>
                        <label>Description</label>
                        <div class="form-line">
                            <textarea rows="4"  name="desc"  value="{$des}" class="form-control no-resize" ></textarea>
                        </div>
                        <br>
                        <label>histoire</label>
                        <div class="form-line">
                            <textarea rows="4" name="his" value="{$his}" class="form-control no-resize" placeholder="Please type what you want..."></textarea>
                        </div>
                        <br>
                        <label>specialité</label>
                        <div class="form-line">
                            <textarea rows="4" name="spe" value="{$spe}" class="form-control no-resize" placeholder="Please type what you want..."></textarea>
                        </div>
                        </br>
                        <div class="body">

                            <div class="dz-message">
                                <div class="drag-icon-cph">
                                    <i class="material-icons">touch_app</i>
                                </div>
                                <h3>Ajouter une image.</h3>

                            </div>
                            <div class="fallback">
                                <input name="image[]" type="file" multiple  id="BSbtnsuccess"/>
                            </div>
                            <br><br>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">



                            </div>
                            <input style="margin-left:800px " type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect" value="Ajouter" name="btnadd">
                        </div>


                        <input type="text" name="idregion"  value=""  hidden />
                        <div class="modal-footer remove-top">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit"  class="btn btn-primary " id="updateButtonConfirm" name="update">Enregistrer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- fin modal update-->

    <!-- modal1 suppression-->
    <div id="us6-dialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#c53430;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Confirmer votre suppression !</h3>
                </div>
                <div class="modal-body">
                    <h4> Voulez vous supprimer cette Region ?</h4>
                    <form method="post" action="delete">
                        {{csrf_field()}}
                        <input type="text" name="idregion"  value=""  hidden/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Non</button>
                    <button  id="deleteButtonConfirm" class="btn btn-danger"  name="oui" >Oui</button>
                </div>
                </form>

            </div>
        </div>
    </div>
    </div>
    <!-- fin modal suppression-->
    <!-- modal1 photo-->
    <div id="us-dialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color:#c53430;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Afficher image!</h3>
                </div>
                <div class="modal-body">

                    <form method="post" action="delete">
                        {{csrf_field()}}
                        <h3>Ajouter une image.</h3>


                <div class="fallback">
                    <input name="image[]" type="file" multiple  id="BSbtnsuccess"/>
                </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Non</button>
                            <button  id="deleteButtonConfirm" class="btn btn-danger"  name="oui" >Oui</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- fin modal photo -->
    <!-- Jquery Core Js -->
    <script src="/js/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="/js/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="/js/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/js/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/js/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="/js/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/js/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/js/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/js/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="/js/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="/js/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="/js/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/js/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/js/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <!-- Custom Js -->
    <script src="/js/admin.js"></script>
    <script src="/js/pages/tables/jquery-datatable.js"></script>
    <!-- Dropzone Plugin Js -->
    <script src="/js/plugins/dropzone/dropzone.js"></script>
    <!-- Demo Js -->

    <script src="/js/demo.js"></script>
    <!-- recuperation data vers modal-->

<script>

    $('#myModal').on('show.bs.modal', function(e) {

        var idreg = $(e.relatedTarget).data('region-id');
        $(e.currentTarget).find('input[name="idregion"]').val(idreg);
    });
    $('#myModal').on('show.bs.modal', function(e) {

        var nom = $(e.relatedTarget).data('region-nom');
        $(e.currentTarget).find('input[name="nomregion"]').val(nom);
    });
    $('#us6-dialog').on('show.bs.modal', function(e) {

        var idreg = $(e.relatedTarget).data('region-id');
        $(e.currentTarget).find('input[name="idregion"]').val(idreg);
    });
    $('#myModal').on('show.bs.modal', function(e) {
        var des = $(e.relatedTarget).data('region-des');
        $(e.currentTarget).find('textarea[name="desc"]').val(des);
    });
    $('#myModal').on('show.bs.modal', function(e) {
        var his = $(e.relatedTarget).data('region-his');
        $(e.currentTarget).find('textarea[name="his"]').val(his);
    });
    $('#myModal').on('show.bs.modal', function(e) {
        var spe = $(e.relatedTarget).data('region-spe');
        $(e.currentTarget).find('textarea[name="spe"]').val(spe);
            });
    $('#us-dialog').on('show.bs.modal', function(e) {

        var ph = $(e.relatedTarget).data('region-photo');
        $(e.currentTarget).find('img[name="image"]').val(ph);
    });
</script>
    <script>
        $(function () {
            var gallery = $('.agileinfo-gallery-row a').simpleLightbox({navText: ['&lsaquo;', '&rsaquo;']});
        });
    </script>
</body>


</html>