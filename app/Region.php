<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
public $table='regions';
public $primaryKey='id';
  public  function photo()
    {
        return $this->hasMany(Photo::class);
    }

}
