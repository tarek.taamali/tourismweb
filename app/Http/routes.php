<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();
//page lister
Route::get('/listerregion',"RegionController@viewregion")->middleware('auth');
//page add
Route::post('/addregion',"RegionController@AddRegion")->middleware('auth');
Route::get('/addregion',"RegionController@AddRegion")->middleware('auth');
// page afficher json
Route::get('/afficher/{id}',"RegionController@listerjson");
//page delete
Route::post('/delete','RegionController@deleteregion')->middleware('auth');
//page edit
Route::post('/edit','RegionController@update')->middleware('auth');
Route::get('/listerjson/{id}',"RegionController@listerjson");

