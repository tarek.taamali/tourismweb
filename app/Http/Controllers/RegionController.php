<?php

namespace App\Http\Controllers;
use Illuminate\Database\Query;

use App\Photo;
use App\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use PhpParser\Node\Expr\Array_;
use App\Http\Controllers\Controller;
use App\Http\Requests\FichePictures;
use Validator;
use Illuminate\Support\Facades\Redirect;
class RegionController extends Controller
{
    public function viewregion()
    {
        $region=Region::all();
        $arr=Array('region'=>$region);
        return view ('lister_region',$arr);
    }

    public function AddRegion( Request $request )
    {

        if($request->isMethod('post')) {

            $newregion = new Region();

            $newregion->nom = $request->input('reg');
            $newregion->description = $request->input('description');
            $newregion->histoire = $request->input('histoire');
            $newregion->specialite = $request->input('specialite');
            $newregion->save();
            $id = $newregion->id;
            foreach ($request->file('image') as $file) {
                $newphotos = new Photo();
                $newphotos->region_id = $id;
                $newphotos->nomphoto = $file->getClientOriginalName();
                // DB::table('photos')->insert(array('id_reg' => $id, 'nomphoto' => $file));
                $newphotos->save();

                if ($newphotos->save()) {
                    $destinationpath = 'uploads';
                    $file->move($destinationpath, $newphotos->nomphoto);

                }
            }
        }
        return view('ajouter_region');
    }


    public function getpic()
    {
       // $id=$request->input('idregion');


        $pid = Photo::find(1);
        $pic=DB::table('photos')->where('region_id','=',$pid)->get();
        return view('lister_region',$pic);

    }



    public function listerjson(Request $request, $id)
    {

        $url ='http://192.168.42.28:8000/uploads/';
        $region=Region::find($id);

        $arr=Photo::select('nomphoto')->join('regions', 'id', '=', 'region_id')->where('region_id','=',$id)->get();


        $ch[]="";
        $i=0;
        foreach ($arr as $myarr){

            $ch[$i]=$url.'' .$myarr->nomphoto  ;


            $i++;

        }


        return array('region' => $region, 'img' => $ch);
    }

    public   function _merge_array($old, $new)
    {
        foreach ($new as $key => $item) {
            if (isset($old[$key])
                && gettype($old[$key]) == gettype($new[$key])
            ) {
                if (is_array($item) || is_object($item)) {
                    $old[$key] = _merge_array($old[$key], $new[$key]);
                } else {
                    $old[$key] = $new[$key];
                }
            }
        }

        return $old;
    }

    public function deleteregion(Request $request)
    {

        $id=$request->input('idregion');
        $region = Region::find($id);
        $region->delete();
        return redirect()->back();

    }
    public function update (Request $request)
    {

        $id=$request->input('idregion');
        $description=$request->input('desc');
        $histoire=$request->input('his');
        $specialite=$request->input('spe');
        $imgs=$request->file('image');

        DB::table('regions')->where('id', '=', $id)
            ->update(array('description' =>$description,'histoire'=>$histoire,'specialite'=>$specialite));

        if (is_array($imgs) && is_object($imgs))
        {

            foreach ( $imgs as $f)
            {
                $newphotos = new Photo();
                $newphotos->region_id = $id;
                $newphotos->nomphoto = $f->getClientOriginalName();
                $newphotos->save();
                if ($newphotos->save())
                {
                    $destinationpath = 'uploads';
                    $f->move($destinationpath,$newphotos->nomphoto);
                }
            }

        }
        return redirect()->back();
    }


}
