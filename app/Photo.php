<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public $table='photos';
    public $primaryKey='id_photo';
  public  function region()
    {
        return $this->belongsTo(Region::class,'id_reg');
    }
}
